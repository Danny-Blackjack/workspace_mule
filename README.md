# README #

This is a very basic Mule connector based on commons-ssh. It allowed me to use an SSH session to many servers, and tail multiple remote files on each server. 

* The mule flow sample has an example on how it can be used.
* SSH implementation is 'com.jcraft.jzlib and jsch', but commons-ssh abstracts the underlaying implementation somewhat.
* Build can be done using 'mvn install'
* This has been used on Mule 3.4.0
* It requires java 6 or 7
 