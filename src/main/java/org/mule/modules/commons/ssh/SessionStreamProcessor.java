package org.mule.modules.commons.ssh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import net.sf.commons.ssh.ExecSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.mule.DefaultMuleEvent;
import org.mule.DefaultMuleMessage;
import org.mule.MessageExchangePattern;
import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.construct.Flow;

/**
 * This processes the session inputStream.
 * 
 * Several methods provide different processing behavior:
 * - run()  streamToCallback() send the result to the callBack flow specified in the callbackFlowName.
 *   It's purpose is asynchronous processing.
 * - streamToPayload() capture the result in a String. It's purpose is synchronous processing.
 * 
 * When all data of the stream is processed, the session and related streams are all closed.
 * 
 */
public class SessionStreamProcessor implements Runnable
{

    private static final Logger  logger            = Logger.getLogger(SessionStreamProcessor.class);

    private MuleContext          muleContext;

    /* Connection details */
    private SshConnectionDetailsVO details;
    /* session with input and output stream */
    private ExecSession          session;
    /* callbackFlow where the ssh session asynchronous output is sent to */
    private Flow                 callbackFlow;
    
    /* More for debugging purposes to know which command this Thread is processing */
    private String               command;
    /* Line counter for StreamToCallBack */
    private int                  linecountSTCB = 0;

    
    /* Platform specific newline character */
    private static final String  NEWLINE           = System.getProperty("line.separator");

    private static final String  SSH_CALLBACK_USER = "SSH_CALLBACK_USER";

    public SessionStreamProcessor(MuleContext muleContext, SshConnectionDetailsVO details, ExecSession session, String command)
    {
        if (details.getCallbackFlowName()!=null) 
        {
            this.callbackFlow = (Flow) muleContext.getRegistry().lookupFlowConstruct(details.getCallbackFlowName());
            if (this.callbackFlow == null)
                throw new IllegalArgumentException("Could not find callback flow with name " + details.getCallbackFlowName());
        }
        this.muleContext = muleContext;
        this.details = details;
        this.session = session;
        this.command = command;
    }

    @Override
    public void run()
    {
        streamToCallback();
    }
    
    /**
     * Send the session stream result to the callBack flow specified in the callbackFlowName.
     * The callbackFlow is invoked for every line received.
     * It's purpose is asynchronous processing.
     */
    public void streamToCallback()
    {
        if (this.callbackFlow == null)
            throw new IllegalArgumentException("Could not find callback flow with name " + details.getCallbackFlowName());

        BufferedReader streamReader = null;
        InputStreamReader insr = null;
        String line = null;
        
        try
        {
            logger.info(details.logString() + " : Start session with callback for '"+command+"'.");
            
            insr = new InputStreamReader(session.getInputStream());
            streamReader = new BufferedReader(insr, details.getReceiverBufferSize());
            
            // Block while we receive data.
            while ( (line=streamReader.readLine()) != null )
            {
                if (line.length() > 0)
                {
                    processCallbackFlow(line);
                    details.setLastAliveTimestamp(System.currentTimeMillis());
                    linecountSTCB++;
                }
            }

        }
        catch (IOException e)
        {
            logger.error(details.logString() + " : Error reading command response. Exception:" + e);
        }
        finally
        {
            // Disconnect properly, releasing resources.
            logger.info(details.logString() + " : Session closed for async command '"+command+"'. Collected "+linecountSTCB+" lines.");
            try
            {
                insr.close();
                streamReader.close();
                session.getInputStream().close();
                session.getOutputStream().close();
                session.close();
            }
            catch (Exception ignore)
            {
                logger.error(details.logString() + " : Error closing streams " + ignore);
            }

            session = null;
            details = null;
            muleContext = null;
            callbackFlow = null;
        }

    }

    /**
     * Send the session stream result to the callBack flow specified in the callbackFlowName.
     * It's purpose is asynchronous processing.
     * The session stream is periodically buffered, and released upon session timeout or when the buffer is full.
     * Note that there are conditions where readLine blocks beyond the session timeout (e.g. when no data arrives).
     * In this case use the non-buffered version.  
     */
    public void streamBufferedToCallback()
    {
        if (this.callbackFlow == null)
            throw new IllegalArgumentException("Could not find callback flow with name " + details.getCallbackFlowName());

        BufferedReader streamReader = null;
        InputStreamReader insr = null;
        int linecount = 0;
        long startts = System.currentTimeMillis();

        try
        {
            logger.info(details.logString() + " : Start session with callback for '"+command+"'.");
            
            insr = new InputStreamReader(session.getInputStream());
            streamReader = new BufferedReader(insr, details.getReceiverBufferSize());
            
            // Do until end of the session
            outer: do
            {
                // Rather than calling the callback for each line, call it with a number of buffered lines
                StringBuffer buffer = new StringBuffer(details.getReceiverBufferSize());
                
                // Block until we receive data.
                String line = streamReader.readLine();

                /*inner:*/ do
                {
                    // null implies end of stream has been reached.
                    if (line == null)
                        break outer;
                    else if (line.length() > 0)
                    {
                        buffer.append(line).append(NEWLINE);
                        linecount++;
                    }
                }
                while( 
                       streamReader.ready() &&                                        // There is at least 1 more line (non-blocking)
                                                                                      //   otherwise exit inner loop and process buffer.
                                                                                      //   Note: session.getInputStream().available() is always 0 for this stream
                       buffer.length() < details.getReceiverBufferSize()  &&          // limit when the buffer is full.
                       (System.currentTimeMillis()-startts < details.getTimeout()) && // limit after timeout 
                       ((line=streamReader.readLine()) != null)                       // Read 1 more line (blocking, but not here)
                                                                                      //   it must be the last statement in the condition.
                                                                                      //   otherwise the line might be lost.
                       ); 
                
                startts = System.currentTimeMillis();
                
                if (buffer.length() > 0) {
                    processCallbackFlow(buffer.toString());
                    details.setLastAliveTimestamp(System.currentTimeMillis());
                }

            } while (!session.isClosed());
        }
        catch (IOException e)
        {
            logger.error(details.logString() + " : Error reading command response. Exception:" + e);
        }
        finally
        {
            // Disconnect properly, releasing resources.
            logger.info(details.logString() + " : Session closed for async command '"+command+"'. Collected "+linecount+" lines.");
            try
            {
                insr.close();
                streamReader.close();
                session.getInputStream().close();
                session.getOutputStream().close();
                session.close();
            }
            catch (Exception ignore)
            {
                logger.error(details.logString() + " : Error closing streams " + ignore);
            }

            session = null;
            details = null;
            muleContext = null;
            callbackFlow = null;
        }

    }

    
    /**
     * Send the session stream result as a payload String. 
     * It's purpose is synchronous processing.
     */
    public String streamToPayload()
    {
        BufferedReader streamReader = null;
        InputStreamReader insr = null;
        int linecount = 0;
        StringBuffer buffer = new StringBuffer(details.getReceiverBufferSize());
        String line = null;
        
        try
        {
            logger.info(details.logString() + " : Start session with payload for '"+command+"'.");

            insr = new InputStreamReader(session.getInputStream());
            streamReader = new BufferedReader(insr, details.getReceiverBufferSize());
            
            // Block while we receive data.
            while ( (line=streamReader.readLine()) != null )
            {
                if (line.length() > 0)
                {
                    buffer.append(line).append(NEWLINE);
                    details.setLastAliveTimestamp(System.currentTimeMillis());
                    linecount++;
                }
            }

        }
        catch (IOException e)
        {
            logger.error(details.logString() + " : Error reading command response. Exception:" + e);
        }
        finally
        {
            // Disconnect properly, releasing resources.
            logger.info(details.logString() + " : Session closed for sync command '"+command+"'. Collected "+linecount+" lines.");
            try
            {
                insr.close();
                streamReader.close();
                session.getInputStream().close();
                session.getOutputStream().close();
                session.close();
            }
            catch (Exception ignore)
            {
                logger.error(details.logString() + " : Error closing streams " + ignore);
            }

            session = null;
            details = null;
        }
        return buffer.toString();
    }
    
    /**
     * sends the message to the responseFlow if not null.
     * 
     * @param message
     *            - the message. If null then this method does nothing
     * 
     * @throws MuleException
     */
    protected void processCallbackFlow(String response)
    {
        if (!StringUtils.isEmpty(response))
        {
            Map<String, Object> inbound = new HashMap<String, Object>();
            inbound.put(SSH_CALLBACK_USER, details.getUsername());

            MuleMessage message = new DefaultMuleMessage(response, inbound, null, null, muleContext);
            message.setOutboundProperty(SSH_CALLBACK_USER, details.getUsername());
            MuleEvent responseEvent = new DefaultMuleEvent(message, MessageExchangePattern.ONE_WAY, callbackFlow);

            try
            {
                callbackFlow.process(responseEvent);
            }
            catch (MuleException e)
            {
                logger.error(details.logString() + " : Error processing callback flow. Exception:" + e);
            }
        }
    }

}
