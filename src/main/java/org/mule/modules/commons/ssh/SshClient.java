package org.mule.modules.commons.ssh;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.mule.api.MuleContext;
import org.mule.util.FileUtils;

import net.sf.commons.ssh.AuthenticationOptions;
import net.sf.commons.ssh.Connection;
import net.sf.commons.ssh.ConnectionFactory;
import net.sf.commons.ssh.ExecSession;
import net.sf.commons.ssh.ExecSessionOptions;
import net.sf.commons.ssh.PasswordAuthenticationOptions;
import net.sf.commons.ssh.PublicKeyAuthenticationOptions;
import net.sf.commons.ssh.jsch.JschConnectionFactory;

/**
 * This class represents a client connected to a remote host.
 * 
 * It basically holds a Session. per each command
 * 
 */
public class SshClient
{

	private static final Logger		logger		= Logger.getLogger(SshClient.class);

	private MuleContext				muleContext	= null;
	private SshConnectionDetailsVO	details		= null;

	private Connection				connection	= null;

	/** Threadpool with 1 Thread per session ouput **/
	static private Executor			threadPool	= Executors.newCachedThreadPool();

	public SshClient(MuleContext muleContext, SshConnectionDetailsVO details)
	{
		this.muleContext = muleContext;
		this.details = details;
	}

	public void connect() throws IOException
	{
		openConnection();
	}

	/**
	 * indicates if the connection is active
	 * @return true if the channel is active
	 */
	public boolean isConnected()
	{
		return connection != null && !connection.isClosed();
	}

	public long getLastAliveTimestamp()
	{
		return details.getLastAliveTimestamp();
	}

	/**
	 * Disconnects the connection and the session if either of them are active
	 */
	public void release()
	{
		logger.info(details.logString() + " : Releasing SSH connection.");
		closeConnection();
	}

    /**
     * This sends a command asynchronously to the host. The result of the command is passed in the flow
     * specified by the callbackFlowName. 
     * 
     * creates/reuses a ssh connection to the host. 
     * After sending the command the connection is kept active. It is up to the flow to release it.
     * the ExecSession, executing the command however is closed when the command completes.
     * 
     * @param command
     *            - the command to execute
     * @throws Exception
     */
    public void sendAsync(String command)
    {
        try
        {
            openConnection();

            ExecSession session = openExecSession(command);
            logger.info(details.logString() + " : sendAsync: '" + command + "' result: '" + session.getExitStatus() + "'");

            SessionStreamProcessor streamToFlowThread = new SessionStreamProcessor(muleContext, details, session, command);
            threadPool.execute(streamToFlowThread);

            // The session is closed at the end in the SessionStreamProcessor.
            // So do not close it here as it would cause the thread to fail.
        }
        catch (Exception e)
        {
            logger.error(details.logString() + " : Error sending command. Exception:'" + e + "'");
        }
    }

    /**
     * This sends a command synchronously to the host. The result of the command is passed back as payload
     * to the Flow.
     * 
     * creates/reuses a ssh connection to the host. 
     * After sending the command the connection is kept active. It is up to the flow to release it.
     * the ExecSession, executing the command however is closed when the command completes.
     * 
     * @param command
     *            the command to execute
     * @return
     *      Command response.            
     *      
     * @throws Exception
     */
    public String sendSync(String command)
    {
        String payload = null;
        
        try
        {
            openConnection();

            ExecSession session = openExecSession(command);
            logger.info(details.logString() + " : sendSync: '" + command + "' result: '" + session.getExitStatus() + "'");

            SessionStreamProcessor processor = new SessionStreamProcessor(muleContext, details, session, command);
            payload = processor.streamToPayload();
            // The session is already closed at the end in SessionStreamProcessor.
        }
        catch (Exception e)
        {
            logger.error(details.logString() + " : Error sending command. Exception:'" + e + "'");
        }
        return payload;
    }

	// ///////////////////////////////////////////////////////////////////////////////
	// COMMONS-SSH METHODS
	// ///////////////////////////////////////////////////////////////////////////////
	protected ConnectionFactory getConnectionFactory()
	{
		ConnectionFactory factory = new JschConnectionFactory();
		factory.setKexTimeout(30000);
		factory.setConnectTimeout(30000);
		// If the timeout expires, a SocketTimeoutException is raised, though the Socket is still valid.
		// The timeout must be > 0. A timeout of zero is interpreted as an infinite timeout.
		factory.setSoTimeout(details.getTimeout());
		return factory;
	}

	/**
	 * Initialize an authentication connection to the SSH server using the 'SshConnectionDetails'.
	 * 
	 * @throws IOException
	 */
	protected void openConnection() throws IOException
	{
	    if (connection!=null && connection.isClosed())
	        release();

		if (connection == null)
		{
			logger.info(details.logString() + " : Opening SSH connection");
			AuthenticationOptions authOptions = getAuthenticationOptions();
			connection = getConnectionFactory().openConnection(details.getHost(), details.getPort(), authOptions);
			logger.info(details.logString() + " : SSH Connected.");
		}
	}

	protected void closeConnection()
	{
		if (connection != null)
		{
			logger.info(details.logString() + " : Closing connection");
			try
			{
				connection.close();
			}
			catch (Exception e)
			{
				logger.error(details.logString() + " : Error closing connection. Exception:'" + e + "'");
			}
			finally
			{
				connection = null;
			}
		}
	}

	/**
	 * Sends the execCommand on the connection and returns an ExecSession.
	 */
	protected ExecSession openExecSession(String execCommand) throws IOException
	{
		logger.info(details.logString() + " : Opening SSH exec session." );
		ExecSessionOptions execSessionOptions = new ExecSessionOptions(execCommand);
		ExecSession session = connection.openExecSession(execSessionOptions);
		// session.exitStatus always seems '-1', or '0' when the session is closed.
		// It doesn't seem related to command execution success or failure.
		return session;
	}

	protected void closeSession(ExecSession session)
	{
		if (session != null)
		{
			try
			{
				logger.info(details.logString() + " : Closing session.");
				session.getInputStream().close();
				session.getOutputStream().close();
				session.close();
			}
			catch (Exception e)
			{
				logger.error(details.logString() + " : Error closing session. Exception:'" + e + "'");
			}
			finally
			{
				session = null;
			}
		}
	}

	public AuthenticationOptions getAuthenticationOptions()
	{
		AuthenticationOptions authOptions = null;
		String pkFilePath = null;
		
		if ((pkFilePath=getPublicKeyFilePath())!=null )
		{
			logger.info(details.logString() + " : Using public key authentication '" + pkFilePath + "'");
			authOptions = new PublicKeyAuthenticationOptions(details.getUsername(),pkFilePath);
		}
		else
		{
			logger.info(details.logString() + " : Using password authentication");
			authOptions = new PasswordAuthenticationOptions(details.getUsername(), details.getPassword());
		}
		
		logger.debug(details.logString() + " : authentication options : " + authOptions.getClass());
		return authOptions;
	}

    ////////////////////////////////////////////////////////////////////////////////
	// UTILITIES
	////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Return a valid public key file path if it can be found without throwing an exception.
	 * @return Valid Full valid public key file path, null otherwise.
	 */
	private String getPublicKeyFilePath()
    {
	    if (StringUtils.isBlank(details.getPublicKeyFileName()))
	        return null;

	    try
        {
	        return FileUtils.getResourcePath(details.getPublicKeyFileName(),this.getClass());
        }
        catch (Exception e) 
        {
            logger.error(details.logString() + " : PublicKey File not found: " + details.getPublicKeyFileName());
        }
        return null;
    }

	/**
	 * Tries to find the root cause. Dumps a Stack trace in case of debug logging
	 */
	public String getRootCause(Exception e)
	{
		if (logger.isDebugEnabled())
			logger.debug("Exception details : " + e.getMessage(), e);
		return ExceptionUtils.getRootCauseMessage(e);
	}

	/**
	 * Sleep for durationInMs
	 */
	public void sleep(long durationInMs)
	{
		try
		{
			if (durationInMs > 0)
			{
				Thread.sleep(durationInMs);
			}
		}
		catch (InterruptedException e)
		{}
	}

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((details == null) ? 0 : details.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SshClient other = (SshClient) obj;
        if (details == null)
        {
            if (other.details != null)
                return false;
        }
        else if (!details.equals(other.details))
            return false;
        return true;
    }

}
