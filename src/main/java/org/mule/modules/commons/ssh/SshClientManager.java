package org.mule.modules.commons.ssh;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.mule.api.MuleContext;

/**
 * Handle SshClient connections abstracting the connector from that.
 * 
 * It lazily initializes the connections on demand and keeps a reusable instance of {@org.mule.modules.commons.ssh.SshClient}
 * to represent the fact that a user can keep the Connection open until released.
 * 
 * This manager can be used at a later stage to handle a request to multiple hosts.
 */
public class SshClientManager
{

	/**
	 * Map with connected SshClients
	 */
	private Map<SshConnectionDetailsVO,SshClient>	clients	= new HashMap<SshConnectionDetailsVO,SshClient>();

	/**
	 * Gets a connection for the SshConnectionDetails. If the connection already exist it's reused.
	 * Otherwise a new one is created.
	 * 
	 * In case of needing a brand new connection opened, then {@link org.mule.modules.commons.ssh.SshConnectionManager.openConnection(SshConnectionDetails)} 
	 * is used and the connection obtained tracked
	 * 
	 * @see org.mule.modules.commons.ssh.SshConnectionManager.openConnection(SshConnectionDetails)
	 * 
	 * @param details
	 *            connection information
	 * @return an active connected and ready to use sshClient
	 * @throws IOException
	 */
	public SshClient getConnection(MuleContext muleContext, SshConnectionDetailsVO details) throws IOException
	{
		SshClient client = clients.get(details);
		if (client == null)
		{
			client = new SshClient(muleContext, details);
			client.connect();
			clients.put(details, client);
		}
		return client;
	}

	/**
	 * if an active connection associated with the SshConnectionDetails exists it will be released after calling this method
	 * 
	 * @param details
	 *            SshConnectionDetails which we want to release.
	 */
	public void release(SshConnectionDetailsVO details)
	{
		SshClient client = clients.get(details);
		if (client != null)
		{
			client.release();
			clients.remove(details);
		}
	}

	/**
	 * Checks if an active connection associated with the username exists
	 * 
	 * @param username
	 *            - the username whose connection we want to check
	 */
	public boolean isConnected(SshConnectionDetailsVO details)
	{
		SshClient client = clients.get(details);
		if (client != null && client.isConnected())
		{
			return true;
		}
		return false;
	}

	public long getLastAliveTimestamp(SshConnectionDetailsVO details)
	{
		SshClient client = clients.get(details);
        if (client != null)
            return client.getLastAliveTimestamp();
        else
            return 0;
	}

	/**
	 * Releases all active connections form all users
	 */
	public void releaseAll()
	{
		for (SshClient client : clients.values())
		{
			client.release();
		}
		clients.clear();
	}
}
