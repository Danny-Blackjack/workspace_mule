package org.mule.modules.commons.ssh;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

/**
 * Value object to carry the connection information
 */
public class SshConnectionDetailsVO
{

    private String	host;
	private int		port;
	private String	username;
	private String	password;
	private String	publicKeyFileName;
	private int		timeout;
	private int		receiverBufferSize;
	private String	callbackFlowName;

	/* Track a timestamp when a last a buffer was received through the SSH connection. */
	private long	lastAliveTimestamp;

	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public int getPort()
	{
		return port;
	}

	public void setPort(int port)
	{
		this.port = port;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getPublicKeyFileName()
	{
		return publicKeyFileName;
	}

	public void setPublicKeyFileName(String publicKeyFileName)
	{
		this.publicKeyFileName = publicKeyFileName;
	}

	public int getTimeout()
	{
		return timeout;
	}

	public void setTimeout(int timeout)
	{
		this.timeout = timeout;
	}

	public int getReceiverBufferSize()
	{
		return receiverBufferSize;
	}

	public void setReceiverBufferSize(int receiverBufferSize)
	{
		this.receiverBufferSize = receiverBufferSize;
	}

	public String getCallbackFlowName()
	{
		return callbackFlowName;
	}

	public void setCallbackFlowName(String callbackFlowName)
	{
		this.callbackFlowName = callbackFlowName;
	}

	public long getLastAliveTimestamp()
	{
		return lastAliveTimestamp;
	}

	public void setLastAliveTimestamp(long lastAliveTimestamp)
	{
		this.lastAliveTimestamp = lastAliveTimestamp;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

	/**
	 * @return connection detail in format user@host:port
	 */
	public String logString()
	{
		return this.getUsername() + "@" + this.getHost() + ":" + this.getPort();
	}
	
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((callbackFlowName == null) ? 0 : callbackFlowName.hashCode());
        result = prime * result + ((host == null) ? 0 : host.hashCode());
        result = prime * result + port;
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SshConnectionDetailsVO other = (SshConnectionDetailsVO) obj;
        if (callbackFlowName == null)
        {
            if (other.callbackFlowName != null)
                return false;
        }
        else if (!callbackFlowName.equals(other.callbackFlowName))
            return false;
        if (host == null)
        {
            if (other.host != null)
                return false;
        }
        else if (!host.equals(other.host))
            return false;
        if (port != other.port)
            return false;
        if (username == null)
        {
            if (other.username != null)
                return false;
        }
        else if (!username.equals(other.username))
            return false;
        return true;
    }
	
}
