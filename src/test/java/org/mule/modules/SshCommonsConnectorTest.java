package org.mule.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.junit.*;
import static org.junit.Assert.*;

import org.mule.api.MessagingException;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.construct.Flow;
import org.mule.util.FileUtils;

public class SshCommonsConnectorTest extends org.mule.tck.junit4.FunctionalTestCase
{
    private static final Logger     logger      = Logger.getLogger(SshCommonsConnectorTest.class);
    
	@BeforeClass
	public static void startup()
	{
		// Mule tests timeout after 60 seconds by default. Increased to 5minutes (300000)
		System.setProperty("mule.test.timeoutSecs", "300000");
	}
	
    @Override
    protected String getConfigResources()
    {
        return "mule-config.xml";
    }

    @Test
    public void testAsync() throws Exception
    {
        MuleMessage muleMessage;
        
        muleMessage = testFlowUtil("testFlow.0.isconnected",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());
        
        muleMessage = testFlowUtil("testFlow.0.async",null);
        assertNotNull(muleMessage);
        assertEquals("UP", muleMessage.getPayloadAsString());
        assertNull(muleMessage.getExceptionPayload());
        
        muleMessage = testFlowUtil("testFlow.0.isconnected",null);
        assertEquals("UP", muleMessage.getPayloadAsString());
        
    	Thread.sleep(60000L);
    	
        muleMessage = testFlowUtil("testFlow.0.isconnected",null);
        assertEquals("STALE", muleMessage.getPayloadAsString());
    	
    	muleMessage = testFlowUtil("release.0",null);
    	
        muleMessage = testFlowUtil("testFlow.0.isconnected",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());

        // lines go to callback.out (not callback.log)
        String resultFile = FileUtils.getResourcePath("target/logs/extractssh.log",this.getClass());
        int count = countInFile(resultFile, "ApcServiceImpl");
        assertTrue("Count '"+count+"' does not match", 0==count);
    }

    @Ignore("Long test to verify long maxstale periods, but takes about 7 minutes")
    @Test
    public void testIsStale() throws Exception
    {
        MuleMessage muleMessage;
        
        muleMessage = testFlowUtil("release.0",null);

        muleMessage = testFlowUtil("testFlow.0.isconnected.300s",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());
        
        System.out.println("**** SEND COMMAND **** ");
        muleMessage = testFlowUtil("testFlow.0.async.singlecommandnooutput",null);
        assertNotNull(muleMessage);
        assertNotNull(muleMessage.getPayloadAsString());
        assertNull(muleMessage.getExceptionPayload());
        
        System.out.println("**** IS CONNECTED 1 **** ");
        muleMessage = testFlowUtil("testFlow.0.isconnected.300s",null);
        assertEquals("UP", muleMessage.getPayloadAsString());
        
        Thread.sleep(120000L);
        System.out.println("**** IS CONNECTED 2 **** ");
        muleMessage = testFlowUtil("testFlow.0.isconnected.300s",null);
        assertEquals("UP", muleMessage.getPayloadAsString());

        Thread.sleep(120000L);
        System.out.println("**** IS CONNECTED 3 **** ");
        muleMessage = testFlowUtil("testFlow.0.isconnected.300s",null);
        assertEquals("UP", muleMessage.getPayloadAsString());
        
        Thread.sleep(120000L);
        System.out.println("**** IS CONNECTED 4 **** ");
        muleMessage = testFlowUtil("testFlow.0.isconnected.300s",null);
        assertEquals("STALE", muleMessage.getPayloadAsString());
        
        System.out.println("**** RELEASE **** ");
        muleMessage = testFlowUtil("release.0",null);
        System.out.println("**** IS CONNECTED 5 **** ");
        muleMessage = testFlowUtil("testFlow.0.isconnected.300s",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());
    }
    
    @Test
    public void testSync() throws Exception
    {
        MuleMessage muleMessage;
        
        muleMessage = testFlowUtil("testFlow.1.isconnected",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());
        
        muleMessage = testFlowUtil("testFlow.1.sync",null);
        assertNotNull(muleMessage);
        // Last command is to extract 5000 lines of the APC log file, which go into the payload .....
        assertNotNull(muleMessage.getPayloadAsString());
        assertEquals(5000, countLines(muleMessage.getPayloadAsString()));
        assertNull(muleMessage.getExceptionPayload());
        
        muleMessage = testFlowUtil("testFlow.1.isconnected",null);
        assertEquals("UP", muleMessage.getPayloadAsString());
        
        muleMessage = testFlowUtil("release.1",null);
        
        muleMessage = testFlowUtil("testFlow.1.isconnected",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());
        
        // ... the payload also gets logged
        String resultFile = FileUtils.getResourcePath("target/logs/extractssh.log",this.getClass());
        int count = countInFile(resultFile, "ApcServiceImpl");
        // payload also goes to callback.log
        assertTrue("Count '"+count+"' does not match", 5000==count);
    }

    @Test
    public void testLargeFile() throws Exception
    {
        MuleMessage muleMessage;
        String resultFile = FileUtils.getResourcePath("target/logs/extractssh.log",this.getClass());
        muleMessage = testFlowUtil("testFlow.2.largefile",null);
        assertNotNull(muleMessage);
        assertNull(muleMessage.getExceptionPayload());
        Thread.sleep(120000L);
        int count = countInFile(resultFile, "ApcServiceImpl");
        // Depending if previous test ran
        assertTrue("Count '"+count+"' does not match", 59494==count || 59494+5000==count);
    }
    
    @Test
    public void testNoCallbackFlowName() throws Exception
    {
        MuleMessage muleMessage;
        // send-async with callbackFlowName configured, causes an Exception:
        //  java.lang.IllegalArgumentException: Could not find callback flow with name null
        // which 'cannot' be tested, due to the async nature of this call.
        muleMessage = testFlowUtil("testFlow.1.nocallback",null);
        assertNotNull(muleMessage);
        assertNull(muleMessage.getExceptionPayload());
    }
   
    /**
     * Test manually e.g. by using following commands in the ci-vha-pie terminal
     * cd /home/jenkins/jobs/pie-svn-trunk/workspace/tools
     * export ANT_HOME="/home/didata/environment/ant";export ANT_OPTS="-Xms128m -Xmx128m -XX:MaxPermSize=128m"
     * ant ejbclient -Dejbargs="[PostpaidService|GetAccountSummary|61414007007|TEST|||1][PostpaidService|SendAccountSummary|61414007007|TEST|||1][BillingService|GetPostpaidAccountSummary|61414007007|TEST|||1][TxtService|SendTxt|0414007007|1512|Formatted Txt Message||1][ApcService|GetAccountSummary|Vodafone|0414007007|||1][ApcService|GetFullAccountDetails|Vodafone|0414007007|||1][ApcService|GetSubscriberHistory|Vodafone|0414007007|||1][ApcService|IsValidVoucherPIN|Vodafone|0414007007|112233445566||1][ApcService|IsValidCCForMSISDN|Vodafone|0414007007|7788|1230|1][PieService|GetFullAccountDetails|Vodafone|0414007007||PAB|1][PieService|GetFullAccountDetails|Vodafone|0414007007|||1][PieService|RechargeAccountForCreditCard|Vodafone|0414007007|7788,1230,1500,CO1|PRC|1][PieService|RegisterCard|Vodafone|0414007007|CCN,EXPDATE,CCV|CCC2|1]"
     * If the jeety emulator is not running, this command should return 13 lines across 3 logfiles.
     * @throws Exception
     */
    @Test
    public void testMultitail() throws Exception
    {
        MuleMessage muleMessage;
        
        muleMessage = testFlowUtil("testFlow.0.isconnected",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());
        
        muleMessage = testFlowUtil("testFlow.0.multitail",null);
        assertNotNull(muleMessage);
        
        muleMessage = testFlowUtil("testFlow.0.isconnected",null);
        assertEquals("UP", muleMessage.getPayloadAsString());
        
        Thread.sleep(120000L);
        
        muleMessage = testFlowUtil("release.0",null);
        
        // Wait 2 seconds for all final lines to be processed.
        Thread.sleep(2000L);
        
        muleMessage = testFlowUtil("testFlow.0.isconnected",null);
        assertEquals("DOWN", muleMessage.getPayloadAsString());

        // lines go to callback.out
    }
    
    @Test
    public void testBadPassword() throws Exception
    {
        boolean exceptionThrown = false;
        try
        {
            testFlowUtil("testFlow.3.badpassword",null);
        }
        catch (Exception e)
        {
            assertTrue(e instanceof MessagingException);
            assertEquals("Auth fail", e.getCause().getMessage());
            exceptionThrown = true;
        }
        Thread.sleep(1000L);
        assertTrue(exceptionThrown);
    }
    
    protected MuleMessage testFlowUtil(String flowName, String sendPayload) throws Exception
    {
        Flow flow = (Flow)getFlowConstruct(flowName);
        MuleEvent sendEvent = getTestEvent(sendPayload);
        MuleEvent responseEvent = flow.process(sendEvent);
        MuleMessage muleMessage = responseEvent.getMessage();
        logger.debug("Response Message  : " + muleMessage);
        logger.debug("Response Payload  : " + muleMessage.getPayloadAsString());
        logger.debug("Response Exception: " + muleMessage.getExceptionPayload());
        return muleMessage;
    }
    
    protected int countLines(String multiline)
    {
        int lines = 0;
        Matcher m = Pattern.compile("(\r\n)|(\n)|(\r)").matcher(multiline);
        while (m.find())
            lines ++;
        return lines;
    }
    
    protected int countInFile(String fileName, String searchString) throws Exception
    {
        if (fileName==null)
            return 0;
        String line = null;
        int counter=0;
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new FileReader(fileName));
            while( (line=reader.readLine())!=null )
            {
                if (line.contains(searchString) ) 
                    counter++;
            }
        }
        catch (Exception ignore) {}
        finally
        {
            if (reader!=null)
                reader.close();
        }
        return counter;
    }
}
